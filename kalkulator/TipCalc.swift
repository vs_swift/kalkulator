//
//  TipCalc.swift
//  kalkulator
//
//  Created by Private on 1/24/18.
//  Copyright © 2018 Private. All rights reserved.
//

import Foundation

class TipCalc {
    var tipAmount: Double = 0
    var amountBeforeTax: Double = 0
    var tipPercentage: Int = 0
    var totalAmount: Double = 0
    
    init(amountBeforeTax: Double, tipPercentage: Int) {
        self.amountBeforeTax = amountBeforeTax
        self.tipPercentage = tipPercentage
    }
    
    func calculateTip() {
        tipAmount = (amountBeforeTax * Double(tipPercentage))/100
        totalAmount = tipAmount + amountBeforeTax
    }
}
