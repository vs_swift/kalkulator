//
//  ViewController.swift
//  kalkulator
//
//  Created by Private on 1/24/18.
//  Copyright © 2018 Private. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var initialAmount: UITextField!
    @IBOutlet weak var tipPercentageLabel: UILabel!
    @IBOutlet weak var tipAmountLAbel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var tipPercentageSlider: UISlider!
    
    var tipCalc = TipCalc(amountBeforeTax: 0.00, tipPercentage: 15)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tipPercentageLabel.text = String(format: "Tip (%d%%)", arguments: [Int(tipCalc.tipPercentage)])
    }
    
    func calcTip() {
        tipCalc.tipPercentage = Int(Double(tipPercentageSlider.value))
        tipCalc.amountBeforeTax = ((initialAmount.text)! as NSString).doubleValue
        tipCalc.calculateTip()
        updateUI()
    }
    
    func roundToFive(n: Double) -> Double {
        let f = floor(n)
        return f + round((n-f) * 20) / 20
    }
    
    func updateUI()
    {
        tipAmountLAbel.text = String(format:"$%0.2f", roundToFive(n: tipCalc.tipAmount))
        totalAmountLabel.text = String(format:"$%0.2f", roundToFive(n: Double(tipCalc.totalAmount)))
    }
    
    @IBAction func initialAmountChanged(_ sender: Any) {
        calcTip()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == initialAmount {
            textField.resignFirstResponder()
            calcTip()
        }
        return true
     }
    
    @IBAction func tipPercentageSliderChanged(_ sender: UISlider) {
        tipPercentageLabel.text! = "Tip (" + String(Int(sender.value)) + "%)"
        calcTip()
        }
}
